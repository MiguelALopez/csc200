class Cat:
  def __init__(self, color, legs, eyes):
    self.color = color
    self.legs = legs
    self.eyes = eyes

felix = Cat("ginger", 4, 2)
rover = Cat("dog-colored", 4, 2)
stumpy = Cat("brown", 3, 3)
meoew = Car("blue", 4, 1)
