from itertools import count

for i in count(0):
  print(i)
  if i >=100:
    break

from itertool import product
from itertool import permutations 
letters = ("A", "B", "C")
print(list(product(letters, range(2))))
print(list(permutations(letters)))
