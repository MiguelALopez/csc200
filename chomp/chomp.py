from gasp import *

GRID_SIZE = 30 
MARGIN = GRID_SIZE

BACKGROUND_COLOR = color.BLACK
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

#   % - Wall
#   . - Food
#   o - Capsule
#   G - Ghost
#   P - Chomp
# Other characters are ignored

the_layout = [
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",     
  "%.....%.................%.....%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.%.....%......%......%.....%.%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%.%%%.%.%%% %%%.%.%%%.%...%",
  "%.%%%.......%GG GG%.......%%%.%",
  "%...%.%%%.%.%%%%%%%.%.%%%.%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%.%.....%......%......%.....%.%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.....%........P........%.....%",
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]

class Maze:
    def __init__(self):
        self.have_window = False      
        self.game_over = False          
        self.set_layout(the_layout)     
        set_speed(20) 
    def set_layout(self, layout):
        height = len(layout)                   # Length of list
        width = len(layout[0])                 # Length of first string
        self.make_window(width, height)
        self.make_map(width, height)           # Start new map

        max_y = height - 1
        for x in range(width):                 # Go through whole layout
            for y in range(height):
                char = layout[max_y - y][x]    
                self.make_object((x, y), char)
    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE     # Work out size of window
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width,           # Create window
                       screen_height,
                       "Chomp",
                       BACKGROUND_COLOR)
    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN    
        y = y*GRID_SIZE + MARGIN     
        return (x, y)
    def make_map(self, width, height):
        self.width = width                 # Store size of layout
        self.height = height
        self.map = []                      # Start with empty list
        for y in range(height):
            new_row = []                   # Make new row list
            for x in range(width):
                new_row.append(Nothing())  # Add entry to list
            self.map.append(new_row)  
    def make_object(self, point, character):
        (x, y) = point
        if character == '%':                    # Is it a wall?
            self.map[y][x] = Wall(self, point)
    def finished(self):
        return self.game_over        # Stop if game is over

    def play(self):
        update_when('next_tick')     # Just pass time at loop rate

    def done(self):
        end_graphics()               # We've finished
        self.map = [] 
    def object_at(self, point):
        (x, y) = point

        if y < 0 or y >= self.height:         # If point is outside maze,
            return Nothing()                  # return nothing.

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]


class Immovable:
    def is_a_wall(self):
        return False                # Most objects aren't walls so say no

class Nothing(Immovable):
    pass

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point                          # Store our position
        self.screen_point = maze.to_screen(point)
        self.maze = maze                            # Keep hold of Maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = GRID_SIZE * 0.2
        # Just draw circle
        Circle(self.screen_point, dot_size, color=WALL_COLOR, filled=True)
    def is_a_wall(self):
        return True 
    def draw(self):
        dot_size = GRID_SIZE * 0.2               # You can remove...
        Circle(self.screen_point, dot_size,      # ...these three lines...
               color = WALL_COLOR, filled = 1)   # ...if you like.
        (x, y) = self.place
        # Make list of our neighbors.
        neighbors = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        # Check each neighbor in turn.
        for neighbor in neighbors:
            self.check_neighbor(neighbor)
    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)            # Get object

        if object.is_a_wall():                       # Is it a wall?
            here = self.screen_point                 # Draw line from here dum
            there = maze.to_screen(neighbor)         
            Line(here, there, color=WALL_COLOR, thickness=2)

class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze                     
        self.place = point                    
        self.speed = speed    
class Chomp(Movable):
    def __init__(self, maze, point):
        Movable.__init__(self, maze, point, CHOMP_SPEED)
    def move(self):
        keys = keys_pressed()
        if   'left' in keys: self.move_left()   # Is left arrow pressed?
        elif 'right' in keys: self.move_right() # Is right arrow pressed?
        elif 'up' in keys: self.move_up()       
        elif 'down' in keys: self.move_down()






the_maze = Maze()
while not the_maze.finished():
    the_maze.play()

the_maze.done()
